For running the admin dashboard run 3 commands given below.
npm install
npm run dev
php artisan migrate:fresh
php artisan serve


output
-------
Laravel development server started: http://127.0.0.1:8000
[Fri Apr 16 18:02:11 2021] PHP 7.4.3 Development Server (http://127.0.0.1:8000) started


Then load http://127.0.0.1:8000 this url in browser.


All Events
------------
For Listing events

Ticket & Pricing button showing popup which containing the ticket prices of the event.
Also there is an edit option.

Create Event
------------

Option for creating an event from backend

fill all details.

There is an add button that will generate new text boxes for enter new event lineups

Ticket price adding text field is dynamically generating based on the ticket types which are added. 

Create booking
--------------
For creating Booking first we need to check user is existing or not.
Enter mobile number and check user exist or not

if exist then choose ticket type 

if not then add user details and submit. Then booking and user registration will complete.

Sales Report
-------------

Listing all bookings


Ticket Types
-----------
For adding types of tickets


