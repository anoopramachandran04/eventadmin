<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@dashboard')->name('home');
Route::get('/events', 'HomeController@eventlist')->name('home');
Route::get('/addEvent', 'HomeController@addEvent')->name('home');
Route::post('/saveEvent', 'HomeController@saveEvent')->name('home');
Route::get('/editEvent/{event}', 'HomeController@editEvent')->name('home');
Route::post('/updateEvent/{event}', 'HomeController@updateEvent')->name('home');

Route::get('/bookings', 'HomeController@bookings')->name('home');
Route::get('/createBooking', 'HomeController@createBooking')->name('home');
Route::post('/saveBooking', 'HomeController@saveBooking')->name('home');

Route::get('/ticketTypes', 'HomeController@ticketTypes')->name('home');
Route::get('/addTicketType', 'HomeController@addTicketType')->name('home');
Route::post('/saveTicketType', 'HomeController@saveTicketType')->name('home');
Route::get('/editTicketType/{type}', 'HomeController@editTicketType')->name('home');
Route::post('/updateTicketType/{type}', 'HomeController@updateTicketType')->name('home');

