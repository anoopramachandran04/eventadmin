<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\AuthController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('checkMobile','ApiController@checkMobile');
Route::post('/getPrice','ApiController@getPrice');
Route::post('/getTicketPricesByEventId','ApiController@getTicketPricesByEventId');

Route::group([
    'middleware' => 'jwt',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});
Route::group(['middleware' => ['jwt.verify']], function() {
    Route::post('/checkUserLogin','ApiController@loginCheck')->middleware('jwt');
});

