<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserManager extends Model
{
    protected $table = 'user_manager';
    public $timestamps = false;
    protected $primaryKey = 'id';
   
}
