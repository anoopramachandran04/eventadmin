<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';
    public $timestamps = false;
    public function getTicketTypes(){

        $ticketTypes = DB::table('ticket_types')->get();
        return $ticketTypes;
    }
    public function add_event_prices($data){
        DB::table('event_prices')->insert(
            array('event_id' => $data['event_id'],'created_date'=>$data['created_date'] , 'price' =>  $data['price'],'ticket_type_id' => $data['ticket_type_id'])
        );
        
    }
    public function update_event_prices($data){
       
        DB::table('event_prices')
        ->where('event_id', $data['event_id']) 
        ->where('ticket_type_id', $data['ticket_type_id']) 
        ->limit(1) 
        ->update(array('price' => $data['price'])); 
        
    }
    public function eventList(){
        return DB::table('events')
        ->select('events.*')
        ->get();
    }
    public function eventListById($id){
        $result = DB::table('events')
        ->leftJoin('event_prices', 'events.id', '=', 'event_prices.event_id')
        ->leftJoin('ticket_types', 'ticket_types.id', '=', 'event_prices.ticket_type_id')
        ->where('events.id', '=', $id)
        ->select('events.*','events.id as eventId','event_prices.price')
        ->get();

        return $result;
    }
   public function getEventTicketAndPrices($event_id){
    $result = DB::table('event_prices')
    ->leftJoin('ticket_types', 'event_prices.ticket_type_id', '=', 'ticket_types.id')
    ->where('event_prices.event_id', '=', $event_id)
    ->select('ticket_types.name','event_prices.price','event_prices.ticket_type_id')
    ->get();
    return $result;
   }
   public function getEventTicketPrice($event_id,$ticket_id){
    $result = DB::table('event_prices')
    ->where('event_prices.event_id', '=', $event_id)
    ->where('event_prices.ticket_type_id', '=', $ticket_id)
    ->select('event_prices.price','event_prices.ticket_type_id')
    ->get();
    return $result;
   }
}
 