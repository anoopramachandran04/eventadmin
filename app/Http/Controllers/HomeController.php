<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Booking;
use App\UserManager;
use App\TicketTypes;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->event = new Event;
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function dashboard()
    {
        return view('dashboard');
    }
    public function addEvent()
    {
        //$types = TicketTypes::all();
        
        $types = $this->event->getTicketTypes();
      
        return view('add_event',['ticketTypes'=>$types]);
    }
    public function editEvent($event_id)
    {
        $event = $this->event->eventListById($event_id);
        $eventTicketPrices = $this->event->getEventTicketAndPrices($event_id);
        $event_lineup = json_decode($event[0]->event_lineup);
        return view('edit_event',['eventTicketPrices'=>$eventTicketPrices , 'data'=>$event,'event_lineup'=>$event_lineup]);
    }
    public function saveEvent(Request $req)
    {

       // echo "<pre>";print_r($timenew);exit;
        $validated = $req->validate([
            'event_name' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'ticket_price' => 'required|array|min:1',
            'title' => 'required|array|min:1',
            'event_time' => 'required|array|min:1',
            'event_description' => 'required',
        ]);
        
        $event = new Event;
        
        $event->event_name = $req->event_name;
        $start_date =date_create($req->start_date);
        $end_date =date_create($req->end_date);
        $event->start_date = date_format($start_date,"Y-m-d H:i:s");
        $event->end_date = date_format($end_date,"Y-m-d H:i:s");
        $event->event_description = $req->event_description;
        $event_lineup = array_combine( $req->event_time, $req->title);
        $event->event_lineup = json_encode($event_lineup);
        $event->created_date = Carbon::now();
        $event->status = 1;
        $event->save();
        $event_id = $event->id;
        $ticket_price = $req->ticket_price;
        $ticketTypeArray = $req->ticketTypeArray;
        $prices = array_combine($ticket_price, $ticketTypeArray);
      
        foreach ($prices as $key=>$row){ 
            $data_arr=array();
            //echo "<pre>";print_r($prices);exit;
            $data_arr['event_id'] = $event_id;
            $data_arr['ticket_type_id'] = $row;
            $data_arr['price'] = $key;
            $data_arr['created_date'] = Carbon::now();
            $this->event->add_event_prices($data_arr);
            }
                return redirect('events')->with('success', 'Added Successfully.');

    }
    public function updateEvent(Request $req, $event)
    {
        $validated = $req->validate([
            'event_name' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'ticket_price' => 'required|array|min:1',
            'title' => 'required|array|min:1',
            'event_time' => 'required|array|min:1',
        ]);
        $event = Event::find($event);
        $event->event_name = $req->event_name;
        $start_date =date_create($req->start_date);
        $end_date =date_create($req->end_date);
        $event->start_date = date_format($start_date,"Y-m-d H:i:s");
        $event->end_date = date_format($end_date,"Y-m-d H:i:s");
        
        $event->event_description = $req->event_description;
        $event_lineup = array_combine( $req->event_time, $req->title);
        $event->event_lineup = json_encode($event_lineup);
        $event->save();

        $event_id = $event->id;
        $ticket_price = $req->ticket_price;
        $ticketTypeArray = $req->ticketTypeArray;
        $prices = array_combine($ticket_price, $ticketTypeArray);
        foreach ($prices as $key=>$row){ 
            $data_arr=array();  
            $data_arr['event_id'] = $event_id;
            $data_arr['ticket_type_id'] = $row;
            $data_arr['price'] = $key;
           
            $this->event->update_event_prices($data_arr);
            }
        return redirect('events')->with('success', 'Updated Successfully.');

    }
    public function addTicketType()
    {
        return view('add_ticket_type');
    }
    public function editTicketType($type)
    {
        $type = TicketTypes::find($type);
        return view('edit_ticket_type',['ticket_type'=>$type]);
    }
    public function saveTicketType(Request $req)
    {

       // echo "<pre>";print_r($timenew);exit;
        $validated = $req->validate([
            'name' => 'required',
        ]);
        
        $type = new TicketTypes;
        
        $type->name = $req->name;
        $type->created_date = Carbon::now();
        $type->status = 1;
        $type->updated_date = Carbon::now();
        $type->save();
        return redirect('ticketTypes')->with('success', 'Added Successfully.');

    }
    public function updateTicketType(Request $req, $type)
    {
        $validated = $req->validate([
            'name' => 'required',
        ]);
        $type = TicketTypes::find($type);
        $type->name = $req->name;
        $type->updated_date = Carbon::now();
        $type->save();
        return redirect('ticketTypes')->with('success', 'Updated Successfully.');

    }
    public function ticketTypes()
    {
         $types = TicketTypes::all();
         return view('ticket_types',['data'=>$types]);
    }
    public function eventlist()
    {
         //$event = Event::all();
         $event = $this->event->eventList();
         $types = TicketTypes::all();
         return view('eventlist',['data'=>$event,'types'=>$types]);
    }
    public function bookings()
    {
         $bookings = Booking::all();
        
                            $result = DB::table('booking')
            ->join('user_manager', 'user_manager.id', '=', 'booking.user_id')
            ->join('events', 'events.id', '=', 'booking.event_id')
            ->select('events.*','booking.*','user_manager.*')
            ->get();
                          
                            return view('bookings',['data'=>$result]);
    }
    public function createBooking()
    {
        $event = Event::all();
        $types = $this->event->getTicketTypes();
        return view('create_booking',['event'=>$event,'ticketTypes'=>$types]);
    }
    public function saveBooking(Request $req)
    {
        $booking = new Booking;
        $user = new UserManager;
        //echo $req->userDetails;exit;
        if($req->userDetails =="null" ){
            
            $validated = $req->validate([
                'first_name' => 'required',
                'last_name' => 'required',
                'mobile' => 'required',
                'email' => 'required'
            ]);
            $user->first_name = $req->first_name;
            $user->last_name = $req->last_name;
            $user->mobile = $req->mobile;
            $user->email = $req->email;
            $user->username = $req->email;
            $user->password = md5($req->mobile);
            $user->created_date = Carbon::now();
            $user->status = 1;
            $user->save();
            $user_id = $user->id;
        }
        else{
           
            $userDetailsDecoded = json_decode($req->userDetails);
            //echo "<pre>";print_r();exit;
            $user_id = $userDetailsDecoded[0]->id;
        }
        $eventDetails = $this->event->getEventTicketAndPrices($req->event,$req->ticket_type);
        $booking->user_id = $user_id;
        $booking->event_id = $req->event;
        $booking->ticket_type = $req->ticket_type;
        $booking->booked_from = "admin";
        $booking->created_date = Carbon::now();
       
      
            $booking->price = $eventDetails[0]->price;
           // echo "<pre>";print_r($booking);exit;
        $booking->save();
        return redirect('bookings')->with('success', 'Added Successfully.');

    }
}
