<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Booking;
use App\UserManager;
use App\Event;

class ApiController extends Controller
{
    //

    public function __construct()
    {
        $this->booking = new Booking;
        $this->event = new Event;

    }
    public function loginCheck(Request $req){


      $userDetails = array();
       $userDetails= UserManager::select('*')
                           ->where('username', '=', $req->username)
                           ->where('password', '=', $req->password)
                           ->get();

         return $userDetails;

    }
    public function checkMobile(Request $req)
    {
    //   echo "<pre>";print_r($req);exit;
    //  //  $this->booking->checkMobileExist($mobile);
    $userDetails = array();
       $userDetails= UserManager::select('*')
                           ->where('mobile', '=', $req->mobile)
                           ->get();

           if($userDetails != '[]'){
            $res['success'] = true;
            $res['userDetails'] = $userDetails;
           }
           else{
            $res['success'] = false;
            $res['userDetails'] =null;
           }
         return $res;
    }
    public function getPrice(Request $req)
    {
    //   echo "<pre>";print_r($req);exit;
    //  //  $this->booking->checkMobileExist($mobile);
    $eventDetails = array();
       $eventDetails=$this->event->getEventTicketPrice($req->event_id,$req->ticket_id);
           if($eventDetails != '[]'){
            $res['success'] = true;
            $res['eventDetails'] = $eventDetails;
           }
           else{
            $res['success'] = false;
            $res['eventDetails'] ="";
           }
         return $res;
    }
    public function getTicketPricesByEventId(Request $req)
    {
     
    $eventDetails = array();
       $eventDetails = $this->event->getEventTicketAndPrices($req->event_id);
           if($eventDetails != '[]'){
            $res['success'] = true;
            $res['eventDetails'] = $eventDetails;
           }
           else{
            $res['success'] = false;
            $res['eventDetails'] ="";
           }
         return $res;
    }
}
