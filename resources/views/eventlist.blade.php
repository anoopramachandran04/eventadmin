@extends('layouts.template')
@section('content')

<div id="page-wrapper" >
            <div id="page-inner">
			 <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">
                            Events
                        </h1>
                    </div>
                </div> 
                 <!-- /. ROW  -->
                 @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
@if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
@endif
            <div class="row"> 
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                             Events List
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover datatable" id="dataTables">
                                    <thead>
                                        <tr>
                                            <th>Event Name</th>
                                            <th>Description</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Tickets & Prices</th>
                                            <th>Edit</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $row)
                                    <tr>
                    <td>
                    {{$row->event_name}}
                    </td>
                    <td class="budget">
                    {{$row->event_description}}
                    </td>
                    <td class="budget">
                    {{$row->start_date}}
                    </td>
                    <td class="budget">
                    {{$row->end_date}}
                    </td>
                    <td class="budget">
                    <a onclick="getTicketPrices('{{$row->id}}');" data-toggle="modal" href="#price_details" class="btn btn-primary"> Tickets & Prices</a>
                    </td>
                    <td class="budget">
                    <a href="editEvent/{{$row->id}}" class="btn btn-primary"><i class="fa fa-edit "></i> Edit</a>
                    </td>
                  </tr>
                 @endforeach



                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>
                <!-- /. ROW  -->
                </div>

                <div class="modal fade" id="price_details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tickets & Prices</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="price_div">
    
    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>
          <script>
              function getTicketPrices(event_id){

               var price_div = '';
$.ajax({
             type:'POST',
             url:'/api/getTicketPricesByEventId',
             data:{event_id:event_id},
             success:function(data) {
               console.log(data.success);
               console.log(data['eventDetails'][0].name);
               if(data.success == true){
                   for(var i=0;i<data['eventDetails'].length;i++){
                    price_div += '<div class="form-group" ><label class="form-control-label" for="input-email">'+data['eventDetails'][i].name+' : '+data['eventDetails'][i].price+'</label></div>'
                   }
                $("#price_div").html(price_div);
               
               
                
               }
               
             }
          });
}
         $(document).ready(function() {
    $('#dataTables').DataTable();
} ); 
</script>
@endsection