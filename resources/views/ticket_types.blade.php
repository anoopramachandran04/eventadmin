@extends('layouts.template')
@section('content')



<div id="page-wrapper" >
            <div id="page-inner">
			 <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">
                            Ticket Types
                        </h1>
                    </div>
                </div> 
                 <!-- /. ROW  -->
                 @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
@if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
@endif
            <div class="row"> 
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                             Ticket Types
                           
                        </div>
                       
                        <div class="panel-body">
                        <span> <a class="btn btn-info btn-sm add-button" href="addTicketType" >Create</a></span>
                        <p>&nbsp;</p>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover datatable" id="dataTables">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Created at</th>
                                            <th>Updated at</th>
                                            <th>Edit</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $row)
                                    <tr>
                    <td>
                    {{$row->name}}
                    </td>
                    <td>
                    {{$row->created_date}}
                    </td>
                    <td>
                    {{$row->updated_date}}
                    </td>
                    <td class="budget">
                    <a href="editTicketType/{{$row->id}}" class="btn btn-primary"><i class="fa fa-edit "></i> Edit</a>
                    </td>
                  </tr>
                 @endforeach



                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>
                <!-- /. ROW  -->
                </div>
          <script>
         $(document).ready(function() {
    $('#dataTables').DataTable();
} ); 
</script>
@endsection