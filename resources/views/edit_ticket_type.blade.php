@extends('layouts.template')
@section('content')
<div id="page-wrapper" >
            <div id="page-inner">
			 <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">
                            Edit Ticket Type
                        </h1>
                    </div>
                </div> 
                @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
                 <!-- /. ROW  -->
              <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          Ticket Types
                        </div>
                        <div class="panel-body">
                            <div class="row">
                            <form method="POST" action="{{url('/updateTicketType/'.$ticket_type->id)}}">
                            @csrf
                                <div class="col-lg-6">
                                  
                                <div class="form-group">
                        <label class="form-control-label" for="input-username">Name</label>
                        <input type="text" id="name" name="name" value="{{$ticket_type->name}}" class="form-control" placeholder="Enter Ticket Type">
                      </div>
                     
                                        
                                      
                                </div>
                               

                                   

                                        <div class="col-lg-6">
                               
                  <div class="form-group">
                  <input type="submit" name="update_type" id="update_type" class="btn btn-primary" value="Update" />
                  </div>

                 
                                        </div>


                               </form>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>

      @endsection