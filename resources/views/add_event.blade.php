@extends('layouts.template')
@section('content')
<div id="page-wrapper" >
            <div id="page-inner">
			 <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">
                            Create Event
                        </h1>
                    </div>
                </div> 
                @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
                 <!-- /. ROW  -->
              <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          Event Details
                        </div>
                        <div class="panel-body">
                            <div class="row">
                            <form method="POST" action="{{url('/saveEvent')}}">
                            @csrf
                                <div class="col-lg-6">
                                  
                                <div class="form-group">
                        <label class="form-control-label" for="input-username">Event Name</label>
                        <input type="text" id="event_name" name="event_name" class="form-control" placeholder="Event Name">
                      </div>
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Start Date</label>
                        <input type="text" id="datepicker" name="start_date" class="form-control" placeholder="Enter Start Date">
                      </div>
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">End Date</label>
                        <input type="text" id="enddatepicker" name="end_date" class="form-control" placeholder="Enter End Date">
                      </div>
                                        
                                      
                                </div>
                                <div class="col-lg-6">
                                @foreach($ticketTypes as $row)
                                <div class="form-group">
                        <label class="form-control-label" for="input-username">{{$row->name}} Ticket Price</label>
                        <input type="hidden" name="ticketTypeArray[]" value="{{$row->id}}"/>
                        <input type="text" id= "ticket_price_{{$row->id}}" name="ticket_price[]" value= "" class="form-control" placeholder="Enter Price">
                      </div>
                      @endforeach
                                        
                                      
                                </div>
                               

                                        <div class="col-lg-12">
                                          
                                       </div>
                                       <div class="col-lg-6">
                                         <h4 class="card-title">Event Lineups</h4>
                                           <div class="mainformEventchild">                                                                        
                                              <div class="row form-row lab_report_div">  
                                                <div class="col-md-6">
                                                  <div class="form-group">
                                                    <label>Title<span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" name="title[]" id="id_title" value="">
                                                  </div>
                                                </div>                                    
                                                <div class="col-md-6">                                                                               
                                                  <div class="form-group">
                                                   <label> Time <span class="text-danger">*</span></label>                                    
                                                   <input type="text" class="form-control" name="event_time[]" id="id_event_time" value="">
                                                  </div>
                                                </div>                                                                            
                                              </div>
                                              
                                            </div>
                                            <div class="form-inline pull-right">                            
                                             <a href="#" class="addEventLineupMore" style="color:green;">
                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i> Add
                                             </a>
                                            </div>  
                                        </div>
                                     

                                        <div class="col-lg-6">
                                <div class="form-group">
                    <label class="form-control-label">Enter Description</label>
                    <textarea rows="4" class="form-control" name="event_description" placeholder="A few words about event ..."></textarea>
                  </div>
                  <div class="form-group">
                  <input type="submit" name="add_event" id="add_event" class="btn btn-primary" value="Create" />
                  </div>

                 
                                        </div>


                               </form>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
          
         
            <script type="text/javascript" src="{{ asset('/assets/js/myscript.js') }}"></script>
      @endsection