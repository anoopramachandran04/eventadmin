@extends('layouts.template')
@section('content')

<div id="page-wrapper" >
            <div id="page-inner">
			 <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">
                            Sales
                        </h1>
                    </div>
                    <div class="col-sm-6">
								<div class="pull-right date-range-picker">
									<div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
										<i class="fa fa-calendar"></i>&nbsp;
										<span></span> <i class="fa fa-caret-down"></i>
									</div>
								</div>							
							</div>
                </div> 
                 <!-- /. ROW  -->
                 @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
@if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
@endif
            <div class="row"> 
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                             Sales Report
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables">
                                    <thead>
                                        <tr>
                                            
                                            <th>Event Name</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Booking Date</th>
                                            <th>Event Start Date</th>
                                            <th>Event End Date</th>
                                            <th>Ticket Price</th>
                                            <th>Email</th>
                                            <th>Mobile</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $row)
                                    <tr>
                                    
                    <td>
                    {{$row->event_name}}
                    </td>
                    <td class="budget">
                    {{$row->first_name}}
                    </td>
                    <td class="budget">
                    {{$row->last_name}}
                    </td>
                    <td class="budget">
                    {{$row->start_date}}
                    </td>
                    <td class="budget">
                    {{$row->created_date}}
                    </td>
                    <td class="budget">
                    {{$row->end_date}}
                    </td>
                    <td class="budget">
                    {{$row->price}}
                    </td>
                    <td class="budget">
                    {{$row->email}}
                    </td>
                    <td class="budget">
                    {{$row->mobile}}
                    </td>
                   
                  </tr>
                 @endforeach



                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>
                <!-- /. ROW  -->
                </div>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script type="text/javascript" src="{{ asset('/assets/js/sales.js') }}"></script>
<script>
         $(document).ready(function() {
    $('#dataTables').DataTable();
} ); 
</script>
@endsection