@extends('layouts.template')
@section('content')
<div id="page-wrapper" >
            <div id="page-inner">
			 <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">
                            Create Booking
                        </h1>
                    </div>
                </div> 
                @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
                 <!-- /. ROW  -->
              <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          Booking Details
                        </div>
                        <div class="panel-body">
                        <form method="POST" action="{{url('/saveBooking')}}">
                        @csrf
                          <div class="row"> 
                           
                             <div class="col-lg-6">
                                 <div class="form-group">
                                 <label class="form-control-label" for="input-email">Mobile</label>
                                 <input type="text" id="mobile" name="mobile" class="form-control" placeholder="Enter Mobile Number">
                                </div>
                                <div class="form-group">
                                <input type="button" name="check_mobile" id="check_mobile" onclick="checkUserExist();" class="btn btn-primary" value="Check" />
                                </div>  
                              </div>
                          </div>
                          <div class="row">
                          <input type="text" name="userDetails" id="userDetails" hidden="hidden" />
                          </div>
                          <div class="row">
                            
                               <div id="newuser_div" style="display:none;">
                                <div class="col-lg-6">
                                  <div class="form-group">
                                     <label class="form-control-label" for="input-username">First Name</label>
                                       <input type="text" id="first_name" name="first_name" class="form-control" placeholder="Enter First Name">
                                  </div>
                                 
                                 </div>
                                 <div class="col-lg-6">
                                 <div class="form-group">
                                       <label class="form-control-label" for="input-email">Last Name</label>
                                       <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Enter Last Name">
                                  </div>
                                 </div>
                                 
                                <div class="col-lg-6">
                                  
                                  <div class="form-group">
                                       <label class="form-control-label" for="input-username">Email id</label>
                                       <input type="text" id="email" name="email" class="form-control" placeholder="Enter Email">
                                  </div>
                              </div>
                              </div>
                         <div class="col-lg-6" id ="ticket_type" style="display:none;">

                         <div class="form-group">
                             <label class="form-control-label" for="input-username">Select Event</label>
                             <select class="form-control" id="event" name="event">
                             @foreach($event as $row)
                                <option value="{{$row->id}}">{{$row->event_name}}</option>
                             @endforeach
                               </select>
                           </div>

                           <div class="form-group">
                             <label class="form-control-label" for="input-username">Ticket Type</label>
                               <select class="form-control" onchange="getPrice();" id="ticket_types" name="ticket_type">
                               @foreach($ticketTypes as $row)
                                <option value="{{$row->id}}">{{$row->name}}</option>
                               
                                @endforeach
                               </select>
                           </div>
                          <div class="form-group" id="price_div" style="display:none;">
                             <label  class="form-control-label" for="input-username">Price</label>
                             <input type="text" id="price" name="price" class="form-control" readonly>

                          </div>
                          </div>

                          <div class="col-lg-12">
                         <div class="form-group">
                          <input type="submit" style="display:none;" name="create_booking" id="create_booking" class="btn btn-primary" value="Book" />
                         </div>  
                        </div>
                               
                       </div>
                       </form>
                       </div>       



                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
<script>

function checkUserExist(){

var mobile = document.getElementById("mobile").value;

$.ajax({
               type:'POST',
               url:'/api/checkMobile',
               data:{mobile:mobile},
               success:function(data) {
                $("#userDetails").val(JSON.stringify(data.userDetails));
                 if(data.success == true){
                  $("#newuser_div").hide();
                 }
                 else{
                  $("#newuser_div").show();
                 }
                 $('#create_booking').show();
                 $('#ticket_type').show();
               }
            });

}
window.onload = function() {
  getPrice();
};
function getPrice(){

  var ticket_id = document.getElementById("ticket_types").value;
  if(ticket_id == ''){
    ticket_id = 1; 
  }
  var event_id = document.getElementById("event").value;
  console.log(ticket_id);
  console.log(event_id);
  $.ajax({
               type:'POST',
               url:'/api/getPrice',
               data:{event_id:event_id,ticket_id:ticket_id},
               success:function(data) {
                 console.log("data");
                 console.log(data);
                 if(data.success == true){
                  $("#price_div").show();
                 $("#price").val(data.eventDetails[0]['price']);
                 }
                 
               }
            });
}
</script>

      @endsection